using SDA.Input;
using SDA.UI;
using System;
using UnityEngine;

namespace SDA.Loop
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private MainMenuView mainMenuView;
        [SerializeField] private StackInput input;

        private MainMenuState mainMenuState;
        private GameState gameState;

        private Action transitionToGameState;

        private IBaseState currentlyActiveState;

        private void Start()
        {
            transitionToGameState += () => ChangeState(gameState);

            mainMenuState = new MainMenuState(transitionToGameState, mainMenuView);
            gameState = new GameState();

            ChangeState(mainMenuState);
        }

        private void Update()
        {
            currentlyActiveState?.UpdateState();
        }

        private void OnDestroy()
        {

        }

        private void ChangeState(IBaseState newState)
        {
            currentlyActiveState?.DisposeState();
            currentlyActiveState = newState;
            currentlyActiveState?.InitState();
        }
    }
}
