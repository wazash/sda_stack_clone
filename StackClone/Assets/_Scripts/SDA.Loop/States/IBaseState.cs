namespace SDA.Loop
{
    public interface IBaseState
    {
        void InitState();
        void UpdateState();
        void DisposeState();
    }
}
