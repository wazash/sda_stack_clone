using SDA.UI;
using System;

namespace SDA.Loop
{
    public class MainMenuState : IBaseState
    {
        private Action transitionToGameState;
        private MainMenuView mainMenuView;

        public MainMenuState(Action transitionToGameState, MainMenuView mainMenuView)
        {
            this.transitionToGameState = transitionToGameState;
            this.mainMenuView = mainMenuView;
        }

        public void InitState()
        {
            mainMenuView.ShowView();
        }

        public void UpdateState()
        {

        }

        public void DisposeState()
        {
            mainMenuView.HideView();
        }
    }
}
