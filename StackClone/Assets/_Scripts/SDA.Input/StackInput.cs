using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace SDA.Input
{
    public class StackInput : MonoBehaviour
    {
        private Action onTap;
        public void OnTapAddListener(Action callback)
        {
            onTap += callback;
        }
        public void ClearListeners()
        {
            onTap = null;
        }

        public void OnTap(InputAction.CallbackContext context)
        {
            if (context.action.WasPerformedThisFrame())
            {
                onTap?.Invoke();
            }
        }
    } 
}
